import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";

import { FeatureProperties } from "../../../models/feature";

interface Props {
  user: FeatureProperties;
  handleClick: (userId: number) => void;
}

function AppListItem({ user, handleClick }: Props) {
  return (
    <ListItem button alignItems="flex-start" onClick={() => handleClick(user.id)}>
      <ListItemAvatar>
        <Avatar alt={user.userName} src={user.avatar} />
      </ListItemAvatar>
      <ListItemText primary={user.userName} secondary={user.email} />
    </ListItem>
  );
}

export default React.memo<Props>(AppListItem);
