import React from "react";
import { List, WindowScroller } from "react-virtualized";

import ListItem from "./LisItem";

import { Feature } from "../../../models/feature";

interface Props {
  features: Feature[];
  handleClick: (userId: number) => void;
}

export default class SideBar extends React.PureComponent<Props> {
  rowRenderer = ({ key, index, style }: { key: string; index: number; style: any }) => {
    const { features, handleClick } = this.props;
    const feature = features[index];
    return (
      <div style={style} key={key}>
        <ListItem handleClick={handleClick} user={feature.properties} />
      </div>
    );
  }

  render() {
    const { features } = this.props;
    return (
      <WindowScroller>
        {({ height }) => (
          <List width={350} height={height} rowCount={features.length} rowHeight={60} rowRenderer={this.rowRenderer} />
        )}
      </WindowScroller>
    );
  }
}
