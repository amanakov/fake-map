import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";

import { Feature } from "../../../models/feature";

interface Props {
  open: boolean;
  handleClose: () => void;
  feature: Feature | null;
}

function PopUp({ open, feature, handleClose }: Props) {
  return (
    <Dialog open={open} onClose={handleClose} aria-labelledby="pop-up-feature" aria-describedby="pop-up-feature">
      <DialogContent>{feature && PopUpContent(feature)}</DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}

function PopUpContent(feature: Feature) {
  return (
    <>
      <DialogContentText>{feature.properties.userName}</DialogContentText>
      <DialogContentText>{feature.properties.email}</DialogContentText>
    </>
  );
}

export default React.memo<Props>(PopUp);
