import React from "react";
import OlFeature from "ol/feature";
import OlPoint from "ol/geom/point";
import OlStyle from "ol/style/style";
import OlStyleCircle from "ol/style/circle";
import OlStyleStroke from "ol/style/stroke";

import Map from "../Map";
import { convertToWebMercatorCoordinates } from "../../../services/coordinates";

import { Feature } from "../../../models/feature";
import { Coordinates } from "../../../models/geolocation";

interface Props {
  features: Feature[];
  coordinates: Coordinates | null;
  setCenter: (coordinates: Coordinates | null) => void;
  handleFeatureClick: (featureId: number, coordinates: Coordinates) => void;
}

function FeaturesMap({ features, ...other }: Props) {
  const olFeatures = features.map(data => {
    const point = new OlPoint(convertToWebMercatorCoordinates(data.geometry.coordinates));
    const feature = new OlFeature(point);
    feature.setId(data.id);
    feature.setStyle(
      new OlStyle({
        // fill: new OlFill({ color: data.properties.color })
        image: new OlStyleCircle({
          stroke: new OlStyleStroke({
            color: data.properties.color
          }),
          radius: 5
        })
      })
    );
    return feature;
  });
  return <Map features={olFeatures} {...other} />;
}

export default React.memo<Props>(FeaturesMap);
