import React from "react";
import OlLayerTile from "ol/layer/tile";
import OlMap from "ol/map";
import OlSourceOSM from "ol/source/osm";
import OlView from "ol/view";
import OlFeature from "ol/feature";
import OlLayerVector from "ol/layer/vector";
import OlSourceVector from "ol/source/vector";

import styles from "./index.module.css";
import { Coordinates } from "../../../models/geolocation";

interface Props {
  features: OlFeature[];
  coordinates: Coordinates | null;
  setCenter: (coordinates: Coordinates | null) => void;
  handleFeatureClick: (featureId: number, coordinates: Coordinates) => void;
}

export default class Map extends React.PureComponent<Props> {
  map: OlMap | null = null;

  componentDidMount() {
    const { features, handleFeatureClick } = this.props;
    this.map = new OlMap({
      layers: [
        new OlLayerTile({
          source: new OlSourceOSM()
        }),
        new OlLayerVector({
          source: new OlSourceVector({ features })
        })
      ],
      target: "map",
      view: new OlView({
        center: [0, 0],
        zoom: 2
      })
    });

    this.map.on("click", e => {
      if (!this.map) {
        return;
      }
      const feature = this.map.forEachFeatureAtPixel((e as any).pixel, feature => {
        const { geometry } = feature.getProperties();
        return {
          coordinates: geometry.flatCoordinates as Coordinates,
          featureId: (feature as OlFeature).getId() as number
        };
      });
      if (!feature) {
        return;
      }
      handleFeatureClick(feature.featureId, feature.coordinates);
    });

    this.map.on("moveend", () => {
      this.props.setCenter(this.getCenter());
    });
  }

  getCenter = (): Coordinates | null => {
    if (!this.map) {
      return null;
    }
    const view = this.map.getView();
    return view.getCenter();
  }

  componentDidUpdate(prevProps: Props) {
    const { coordinates } = this.props;
    const shouldSetCenter = !isSameCoordinates(this.props.coordinates, prevProps.coordinates);
    if (shouldSetCenter && coordinates) {
      this.setCenter(coordinates);
    }
  }

  setCenter = (coordinates: Coordinates) => {
    if (this.map) {
      const view = this.map.getView();
      view.setCenter(coordinates);
    }
  }

  render() {
    return (
      <div className={styles.map_container}>
        <div id="map" ref="olmap" className={styles.map} />
      </div>
    );
  }
}

function isSameCoordinates(coordinates1: Coordinates | null, coordinates2: Coordinates | null): boolean {
  if (coordinates1 === coordinates2) {
    return true;
  }
  if (coordinates1 == null || coordinates2 == null) {
    return false;
  }
  return coordinates1[0] === coordinates2[0] && coordinates1[1] === coordinates2[1];
}
