import React from "react";
import { connect } from "react-redux";

import { State as StoreState } from "../../reducers/features";
import { Store } from "../../reducers";
import { fetchFeaturesAction } from "../../actions/features";
import { setPopUpFeatureId } from "../../actions/features/sync";
import { convertToWebMercatorCoordinates } from "../../services/coordinates";
import styles from "./index.module.css";

import Loading from "./Loading";
import FeaturesMap from "./FeaturesMap";
import SideBar from "./SideBar";
import PopUp from "./PopUp";

import { Feature } from "../../models/feature";
import { Coordinates } from "../../models/geolocation";

interface Props extends MapStateToProps, MapDispathToProps {}

class State {
  readonly centerCoordinates: Coordinates | null = null;
}

class Geolocation extends React.PureComponent<Props, State> {
  readonly state = new State();

  componentDidMount() {
    this.props.fetchFeatures();
  }

  handleFeatureClick = (featureId: number, coordinates: Coordinates) => {
    this.props.setPopUpFeatureId(featureId);
    this.setCenter(coordinates);
  }

  handleUserClick = (userId: number) => {
    const { features } = this.props;
    if (!features) {
      return;
    }
    const feature = features.find(feature => feature.properties.id === userId);
    if (!feature) {
      return;
    }
    this.props.setPopUpFeatureId(feature.id);
    const coordinates = convertToWebMercatorCoordinates(feature.geometry.coordinates);
    this.setCenter(coordinates);
  }

  handlePopUpClose = () => {
    this.props.setPopUpFeatureId(null);
  }

  setCenter = (coordinates: Coordinates | null) => {
    this.setState({ centerCoordinates: coordinates });
  }

  render() {
    const { inProgress, error, features, popUpFeature } = this.props;
    const { centerCoordinates } = this.state;
    if (inProgress) {
      return <Loading />;
    }

    if (error) {
      return <h1>{error.message}</h1>;
    }

    if (features) {
      return (
        <div className={styles.geolocation}>
          <SideBar features={features} handleClick={this.handleUserClick} />
          <FeaturesMap features={features} coordinates={centerCoordinates} setCenter={this.setCenter} handleFeatureClick={this.handleFeatureClick} />
          <PopUp feature={popUpFeature} open={!!popUpFeature} handleClose={this.handlePopUpClose} />
        </div>
      );
    }
    return null;
  }
}

interface MapStateToProps extends StoreState {
  popUpFeature: Feature | null;
}

const mapStateToProps = ({ features }: Store): MapStateToProps => {
  return {
    ...features,
    popUpFeature: fetureSelector(features.features, features.popUpFeatureId)
  };
};

function fetureSelector(features: Feature[] | null, fetureId: number | null): Feature | null {
  if (features == null || fetureId == null) {
    return null;
  }
  return features.find(feature => feature.id === fetureId) || null;
}

interface MapDispathToProps {
  fetchFeatures: () => void;
  setPopUpFeatureId: (featureId: number | null) => void;
}

const mapDispathToProps = (dispatch: (action: any) => void): MapDispathToProps => {
  return {
    fetchFeatures: () => dispatch(fetchFeaturesAction()),
    setPopUpFeatureId: featureId => dispatch(setPopUpFeatureId(featureId))
  };
};

export default connect<MapStateToProps, MapDispathToProps, {}, Store>(
  mapStateToProps,
  mapDispathToProps
)(Geolocation);
