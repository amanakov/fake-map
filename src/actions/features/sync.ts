import { createAction } from "typesafe-actions";
import { Feature } from "../../models/feature";

export const setInProgressAction = createAction("features/set-in-progress", resolve => {
  return (inProgress: boolean) => resolve(inProgress);
});

export const setErrorAction = createAction("features/set-error", resolve => {
  return (error: Error | null) => resolve(error);
});

export const setFeaturesAction = createAction("features/set-features", resolve => {
  return (features: Feature[] | null) => resolve(features);
});

export const setPopUpFeatureId = createAction("features/set-pop-up-feature-id", resolve => {
  return (featureId: number | null) => resolve(featureId);
});