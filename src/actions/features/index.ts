import { ActionType } from "typesafe-actions";
import * as actions from "./sync";
import { Dispatch } from "redux";

import { featuresRequest } from "../../requests/features";

export type ActionTypes = ActionType<typeof actions>;

export const fetchFeaturesAction = () => async (dispatch: Dispatch<ActionTypes>) => {
  try {
    dispatch(actions.setInProgressAction(true));
    const { features } = await featuresRequest();
    dispatch(actions.setFeaturesAction(features));
    dispatch(actions.setErrorAction(null));
  } catch (error) {
    dispatch(actions.setErrorAction(error));
  } finally {
    dispatch(actions.setInProgressAction(false));
  }
};
