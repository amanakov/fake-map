import { request } from "../services/api";
import { Feature } from "../models/feature";

interface Response {
  type: "FeatureCollection";
  features: Feature[];
}

export const featuresRequest = () => request<Response>(`/features`);
