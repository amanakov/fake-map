import { combineReducers } from "redux";

import { reducer as features, State as Features } from "./features";

export interface Store {
  readonly features: Features;
}

const reducer = combineReducers<Store>({
  features
});

export default reducer;
