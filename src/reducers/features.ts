import { ActionTypes } from "../actions/features";
import { Reducer } from "redux";
import { Feature } from "../models/feature";

export class State {
  readonly features: Feature[] | null = null;
  readonly inProgress: boolean = false;
  readonly error: Error | null = null;
  readonly popUpFeatureId: number | null = null;
}

export const reducer: Reducer<State, ActionTypes> = (state = { ...new State() }, action: ActionTypes) => {
  if (action.type === "features/set-features") {
    return { ...state, features: action.payload };
  }

  if (action.type === "features/set-in-progress") {
    return { ...state, inProgress: action.payload };
  }

  if (action.type === "features/set-error") {
    return { ...state, error: action.payload };
  }

  if (action.type === "features/set-pop-up-feature-id") {
    return { ...state, popUpFeatureId: action.payload };
  }

  return state;
};
