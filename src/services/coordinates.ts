import OlProj from "ol/proj";

import { Coordinates } from "../models/geolocation";

export function convertToWebMercatorCoordinates(coordinates: Coordinates): Coordinates {
  return OlProj.fromLonLat(coordinates);
}
