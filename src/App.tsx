import React, { Component } from "react";
import Geolocation from "./modules/Geolocation";

import { Provider } from "react-redux";
import store from "./store";

class App extends Component {
  render() {
    return <Geolocation />;
  }
}

export default () => (
  <Provider store={store}>
    <App />
  </Provider>
);
