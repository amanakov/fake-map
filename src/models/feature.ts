import { Coordinates } from "./geolocation";

export interface Feature {
  type: "Feature";
  id: number;
  geometry: FeatureGeometry;
  properties: FeatureProperties;
}

export interface FeatureGeometry {
  type: "Point";
  coordinates: Coordinates;
}

export interface FeatureProperties {
  id: number;
  userName: string;
  avatar: string;
  email: string;
  country: string;
  city: string;
  color: string;
}
